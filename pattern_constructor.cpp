#include "pattern_constructor.h"

const std::string &PatternConstructor::getOutputPattern() const {
  return outputPattern_;
}

int PatternConstructor::getCapturersCount() const { return capturersCount_; }

PatternConstructor::PatternConstructor(const std::string &inputPattern) {
  // the code below works like FSM with actions on transition
  PatternConstructor::ParserState (
      PatternConstructor::*fsmActions[PS_STATES_NUMBER])(void);
  // actions and states binding
  fsmActions[PS_LOOK_FOR_PERCENT]                   = &PatternConstructor::lookForPercent;
  fsmActions[PS_LOOK_FOR_BRACKET_BACKSLASH]         = &PatternConstructor::lookForBracketBackslash;
  fsmActions[PS_LOOK_FOR_BRACKET]                   = &PatternConstructor::lookForBracket;
  fsmActions[PS_LOOK_FOR_TOKEN_NUMBER]              = &PatternConstructor::lookForTokenNumber;
  fsmActions[PS_LOOK_FOR_MODIFIER]                  = &PatternConstructor::lookForModifier;
  fsmActions[PS_LOOK_FOR_CLOSING_BRACKET_BACKSLASH] = &PatternConstructor::lookForClosingBracketBackslash;
  fsmActions[PS_LOOK_FOR_CLOSING_BRACKET]           = &PatternConstructor::lookForClosingBracket;
  fsmActions[PS_LOOK_FOR_SPACE_LIMIT]               = &PatternConstructor::lookForSpaceLimit;
  fsmActions[PS_WRONG_PATTERN]                      = &PatternConstructor::wrongPattern;

  auto escapedPattern = makeEscaped(inputPattern);

  inputIterator_ = escapedPattern.begin();
  sourceStringEnd_ = escapedPattern.end();

  PatternConstructor::ParserState state = PS_LOOK_FOR_PERCENT;

  while (inputIterator_ != sourceStringEnd_) {
    state = (this->*fsmActions[state])();
  }

  // after the string end some unfinnished patterns could be
  // in buffers
  outputPattern_ += stringBuffer_;
  outputPattern_ += tokenNumberBuffer_;
}

std::string PatternConstructor::constructPcre(const Subpattern subpattern) {
  std::string outputString;
  capturersCount_++;
  if (subpattern.type == ST_LAZY) {
    if (!subpattern.isLastSymbol) {
      // If there aren't any symbols after the token, it must be greedy
      // according to the example from the task
      outputString = "(.*?)";
    } else {
      outputString = "(.*)";
    }
  }
  if (subpattern.type == ST_LAZY_WITH_SPACES) {
    outputString =
        "((?:[^ ]*[ ]){" + std::to_string(subpattern.spaceCount) + "}[^ ]*)";
  }

  if (subpattern.type == ST_GREEDY) {
    outputString = "(.*)";
  }
  return outputString;
}

bool PatternConstructor::isSpecial(const char symbol) {
  // pcre special symbols must be escaped
  static const std::string specialSymbols = ".^$*+-?()[]{}\\|";
  return specialSymbols.find(symbol) != std::string::npos;
}

std::string PatternConstructor::makeEscaped(std::string const &input) {
  std::string result;
  for (char c : input) {
    if (isSpecial(c)) {
      result.push_back('\\');
    }
    result.push_back(c);
  }
  return result;
}

void PatternConstructor::constructAndAppend() {
  if (inputIterator_ == sourceStringEnd_) {
    subpattern_.isLastSymbol = true;
  } else {
    subpattern_.isLastSymbol = false;
  }
  auto formattedSubpattern_ = constructPcre(subpattern_);
  outputPattern_ += formattedSubpattern_;
  stringBuffer_.clear();
  spaceLimitBuffer_.clear();
  tokenNumberBuffer_.clear();
}

PatternConstructor::ParserState PatternConstructor::lookForPercent() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  if (*inputIterator_ == '%') {
    newState = PS_LOOK_FOR_BRACKET_BACKSLASH;
  } else {
    newState = PS_WRONG_PATTERN;
  }
  ++inputIterator_;
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForBracketBackslash() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  if (*inputIterator_ == '\\') {
    newState = PS_LOOK_FOR_BRACKET;
  } else {
    newState = PS_WRONG_PATTERN;
  }
  ++inputIterator_;
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForBracket() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  if (*inputIterator_ == '{') {
    newState = PS_LOOK_FOR_TOKEN_NUMBER;
  } else {
    newState = PS_WRONG_PATTERN;
  }
  ++inputIterator_;
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForTokenNumber() {
  PatternConstructor::ParserState newState;
  if (isdigit(*inputIterator_)) {
    tokenNumberBuffer_.push_back(*inputIterator_);
    ++inputIterator_;
    newState = PS_LOOK_FOR_TOKEN_NUMBER;
  } else {
    if (!tokenNumberBuffer_.empty()) {
      // nothing to do with this number according to the task
      // I left it here to use in the future
      // int tokenNumber = std::stoi(token_number_buffer);
      stringBuffer_ += tokenNumberBuffer_;
      newState = PS_LOOK_FOR_MODIFIER;
    } else {
      newState = PS_WRONG_PATTERN;
    }
  }
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForModifier() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  switch (*inputIterator_) {
    case '\\':
      subpattern_.type = ST_LAZY;
      newState = PS_LOOK_FOR_CLOSING_BRACKET;
      break;
    case 'S':
      subpattern_.type = ST_LAZY_WITH_SPACES;
      newState = PS_LOOK_FOR_SPACE_LIMIT;
      break;
    case 'G':
      subpattern_.type = ST_GREEDY;
      newState = PS_LOOK_FOR_CLOSING_BRACKET_BACKSLASH;
      break;
    default:
      newState = PS_WRONG_PATTERN;
  }
  ++inputIterator_;
  return newState;
}

PatternConstructor::ParserState
PatternConstructor::lookForClosingBracketBackslash() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  if (*inputIterator_ == '\\') {
    newState = PS_LOOK_FOR_CLOSING_BRACKET;
  } else {
    newState = PS_WRONG_PATTERN;
  }
  ++inputIterator_;
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForClosingBracket() {
  stringBuffer_.push_back(*inputIterator_);
  PatternConstructor::ParserState newState;
  if (*inputIterator_ == '}') {
    ++inputIterator_;
    constructAndAppend();
    newState = PS_LOOK_FOR_PERCENT;
  } else {
    ++inputIterator_;
    newState = PS_WRONG_PATTERN;
  }
  return newState;
}

PatternConstructor::ParserState PatternConstructor::lookForSpaceLimit() {
  PatternConstructor::ParserState newState;
  if (isdigit(*inputIterator_)) {
    spaceLimitBuffer_.push_back(*inputIterator_);
    ++inputIterator_;
    newState = PS_LOOK_FOR_SPACE_LIMIT;
  } else {
    if (!spaceLimitBuffer_.empty()) {
      stringBuffer_ += spaceLimitBuffer_;
      subpattern_.spaceCount = std::stoi(spaceLimitBuffer_);
      spaceLimitBuffer_.clear();
      newState = PS_LOOK_FOR_CLOSING_BRACKET_BACKSLASH;
    } else {
      newState = PS_WRONG_PATTERN;
    }
  }
  return newState;
}

PatternConstructor::ParserState PatternConstructor::wrongPattern() {
  outputPattern_ += stringBuffer_;
  stringBuffer_.clear();
  spaceLimitBuffer_.clear();
  tokenNumberBuffer_.clear();
  return PS_LOOK_FOR_PERCENT;
}
