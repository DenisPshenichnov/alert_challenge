MAIN_SOURCES=main.cpp pattern_constructor.cpp processor.cpp
TEST_SOURCES=test_pattern_constructor.cpp pattern_constructor.cpp processor.cpp
MAIN_OBJECTS=$(MAIN_SOURCES:.cpp=.o)
TEST_OBJECTS=$(TEST_SOURCES:.cpp=.o)	
MAIN_EXECUTABLE=main
TEST_EXECUTABLE=test_exec

compile: $(MAIN_SOURCES) $(MAIN_EXECUTABLE)

test: 	$(TEST_SOURCES) $(TEST_EXECUTABLE)
		./$(TEST_EXECUTABLE)

$(MAIN_EXECUTABLE): $(MAIN_OBJECTS) 
	$(CXX) $(MAIN_OBJECTS) -o $@ -lpcre $(LDFLAGS)

$(TEST_EXECUTABLE): $(TEST_OBJECTS) 
	$(CXX) $(TEST_OBJECTS) -o $@ -lpcre -lcppunit $(LDFLAGS)

.cpp.o:
	$(CXX) -Wall -c -std=c++11 $(CXXFLAGS) $< -o $@

clean:
	rm -rf *.o $(MAIN_EXECUTABLE) $(TEST_EXECUTABLE)
