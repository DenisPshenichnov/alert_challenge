#include <assert.h>
#include <iostream>

#include "pattern_constructor.h"
#include "processor.h"

Processor::Processor(const std::string &inputPattern) {
  PatternConstructor patternConstructor(inputPattern);

  // 3 elements per each subpattern and 3 for full match
  ovectorSize_ = (patternConstructor.getCapturersCount() + 1) * 3;

  const std::string rawPcrePattern = patternConstructor.getOutputPattern();

  const int options = 0;
  const char *error;
  const char *pattern_c = rawPcrePattern.c_str();
  int erroffset;

  pcreCompiledPattern_ =
      pcre_compile(pattern_c, options, &error, &erroffset, NULL);
  // if the pattern is no compiled it's not necessary to
  // throw an exception, we should terminate the app
  assert(pcreCompiledPattern_ != nullptr);
  free((void *)error);
}

std::vector<std::string> Processor::process(const std::string &string) {
  std::vector<std::string> processed;

  int count = 0;

  // two pointer for one matched (start pos and end pos)
  int ovector[ovectorSize_];

  count = pcre_exec(pcreCompiledPattern_, NULL, string.c_str(), string.size(),
                    0, 0, ovector, ovectorSize_);
  for (int i = 0; i < count * 2; i = i + 2) {
    std::string matchedString = std::string(string.c_str() + ovector[i],
                                            string.c_str() + ovector[i + 1]);
    processed.emplace_back(matchedString);
  }
  return processed;
}

bool Processor::isFullyMatched(const std::string &string) {
  std::vector<std::string> processed = process(string);
  return !processed.empty() && processed[0] == string;
}

Processor::~Processor() { free(pcreCompiledPattern_); }
