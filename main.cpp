#include <iostream>
#include <string>

#include "processor.h"

int main(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << " PATTERN\n";
    return 1;
  }
  Processor processor(argv[1]);
  std::string line;
  while (std::getline(std::cin, line)) {
    // nothing to do with captured sequences,
    // anyway it's possble to use 'Processor::process'
    // instead to get matched sequences
    if (processor.isFullyMatched(line)) {
      std::cout << line << "\n";
    }
  }
}
