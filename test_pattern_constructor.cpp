#include <iostream>
#include <string>
#include <vector>

#include <cppunit/TestCase.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "pattern_constructor.h"
#include "processor.h"

// some simple tests

class PatternConstructorTest : public CppUnit::TestCase {
 public:
  void runTest() {
    test_lazy();
    test_last_lazy_to_greedy();
    test_unfinished_pattern();
    test_unfinished_pattern();
    test_greedy();
    test_special_pcre_symbols();
    test_spaces_limitation();
  }
 protected:
  void test_lazy() {
    // arrange
    const std::string sourcePattern = "%{0}foo";
    std::string expected = "(.*?)foo";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }

  void test_last_lazy_to_greedy() {
    // arrange
    const std::string sourcePattern = "%{0}foo%{1}";
    std::string expected = "(.*?)foo(.*)";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }

  void test_unfinished_pattern() {
    // arrange
    const std::string sourcePattern = "%{0";
    std::string expected = "%\\{0";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }

  void test_greedy() {
    // arrange
    const std::string sourcePattern = "%{0G}foo";
    std::string expected = "(.*)foo";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }

  void test_special_pcre_symbols() {
    // arrange
    const std::string sourcePattern = "*foo{}bar";
    std::string expected = "\\*foo\\{\\}bar";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }

  void test_spaces_limitation() {
    // arrange
    const std::string sourcePattern = "foo%{0S10}";
    std::string expected = "foo((?:[^ ]*[ ]){10}[^ ]*)";
    // act
    PatternConstructor patternConstructor(sourcePattern);
    std::string actual = patternConstructor.getOutputPattern();
    // assert
    CPPUNIT_ASSERT_EQUAL(expected, actual);
  }
};

// test cases from the task
class ProcessorTest : public CppUnit::TestCase {
 public:
  void runTest() {
    test_lazy();
    test_lazy_with_spaces();
    test_greedy();
  }
  void test_lazy() {
    Processor processor = Processor("foo %{0} is a %{1}");
    std::size_t expectedCountSuccess = 3;
    std::size_t expectedCountUnsuccess = 0;
    std::vector<std::string> expectedVectorSuccess = {"foo blah is a bar",
                                                      "blah", "bar"};

    std::vector<std::string> matchedVectorSuccess =
        processor.process("foo blah is a bar");
    std::vector<std::string> matchedVectorUnsuccess =
        processor.process("foo blah is bar");

    CPPUNIT_ASSERT_EQUAL(expectedCountSuccess, matchedVectorSuccess.size());
    // repitation to get a line number on failure
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(0),
                         matchedVectorSuccess.at(0));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(1),
                         matchedVectorSuccess.at(1));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(2),
                         matchedVectorSuccess.at(2));
    CPPUNIT_ASSERT_EQUAL(expectedCountUnsuccess, matchedVectorUnsuccess.size());
  }

  void test_lazy_with_spaces() {
    Processor processor = Processor("the %{0S1} %{1} ran away");
    std::vector<std::string> expectedVectorSuccess = {
        "the big brown fox ran away", "big brown", "fox"};

    std::vector<std::string> matchedVectorSuccess =
        processor.process("the big brown fox ran away");

    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(0),
                         matchedVectorSuccess.at(0));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(1),
                         matchedVectorSuccess.at(1));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(2),
                         matchedVectorSuccess.at(2));
  }

  void test_greedy() {
    Processor processor = Processor("bar %{0G} foo %{1}");
    std::vector<std::string> expectedVectorSuccess = {
        "bar foo bar foo bar foo bar foo", "foo bar foo bar", "bar foo"};

    std::vector<std::string> matchedVectorSuccess =
        processor.process("bar foo bar foo bar foo bar foo");

    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(0),
                         matchedVectorSuccess.at(0));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(1),
                         matchedVectorSuccess.at(1));
    CPPUNIT_ASSERT_EQUAL(expectedVectorSuccess.at(2),
                         matchedVectorSuccess.at(2));
  }
};

int main() {
  PatternConstructorTest patternConstructorTest;
  ProcessorTest processorTest;

  CppUnit::TextTestRunner runner;
  runner.addTest(&patternConstructorTest);
  runner.addTest(&processorTest);
  runner.run();
  return 0;
}
