#ifndef PATTERN_CONSTRUCTOR_H
#define PATTERN_CONSTRUCTOR_H

#include <algorithm>
#include <sstream>
#include <string>

class PatternConstructor {
 public:
  explicit PatternConstructor(const std::string &inputPattern);
  const std::string &getOutputPattern() const;
  int getCapturersCount() const;

 private:
  enum SubpatternType { ST_GREEDY, ST_LAZY, ST_LAZY_WITH_SPACES };

  enum ParserState {
    PS_LOOK_FOR_PERCENT,
    PS_LOOK_FOR_BRACKET_BACKSLASH,
    PS_LOOK_FOR_BRACKET,
    PS_LOOK_FOR_TOKEN_NUMBER,
    PS_LOOK_FOR_MODIFIER,
    PS_LOOK_FOR_CLOSING_BRACKET_BACKSLASH,
    PS_LOOK_FOR_CLOSING_BRACKET,
    PS_LOOK_FOR_SPACE_LIMIT,
    PS_WRONG_PATTERN,
    PS_STATES_NUMBER
  };

  struct Subpattern {
    SubpatternType type;
    int spaceCount;
    bool isLastSymbol;
  } subpattern_;

  bool isSpecial(char symbol);
  std::string makeEscaped(std::string const &input);
  std::string constructPcre(Subpattern subpattern);
  void constructAndAppend();

  // methods below are strongly coupled to a
  // calling code, but it's acceptable for
  // actions in transition in FSM implementation
  ParserState lookForPercent();
  ParserState lookForBracketBackslash();
  ParserState lookForBracket();
  ParserState lookForTokenNumber();
  ParserState lookForModifier();
  ParserState lookForClosingBracketBackslash();
  ParserState lookForClosingBracket();
  ParserState lookForSpaceLimit();
  ParserState wrongPattern();

  std::string stringBuffer_;
  std::string spaceLimitBuffer_;
  std::string tokenNumberBuffer_;
  std::string outputPattern_;
  std::string::iterator inputIterator_;
  std::string::iterator sourceStringEnd_;
  int capturersCount_ = 0;
};

#endif
