#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <string>
#include <vector>

#include <pcre.h>

class Processor {
 public:
  explicit Processor(const std::string& inputPattern);
  std::vector<std::string> process(const std::string& string);
  ~Processor();
  bool isFullyMatched(const std::string& string);

 private:
  pcre* pcreCompiledPattern_;
  int ovectorSize_;
};

#endif
